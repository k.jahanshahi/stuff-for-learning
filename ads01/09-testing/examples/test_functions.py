import unittest
from functions import mult


class TestMult(unittest.TestCase):

    def test_mult(self):
        result = mult(3, 4)
        self.assertEqual(12, result)

    def test_mult_negative(self):
        result = mult(-1, 7)
        self.assertEqual(-7, result)
